class AddColumnToUserAgitId < ActiveRecord::Migration
  def change
    add_column :users, :agit_id, :string

    add_index :users, [:agit_id]
  end
end
