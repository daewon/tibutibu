class AddColumToBrand < ActiveRecord::Migration
  def change
    add_column :brands, :max_coupon_count, :integer, default: 40
  end
end
