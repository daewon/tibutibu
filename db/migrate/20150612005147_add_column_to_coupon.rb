class AddColumnToCoupon < ActiveRecord::Migration
  def change
    add_reference :coupons, :brand, index: true
  end
end
