class CreateCoupon < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.references :user, null: false
      t.references :tibu_tibu, null: false
      t.integer :count, default: 1

      t.timestamps
    end

    add_index :coupons, [:user_id]
    add_index :coupons, [:tibu_tibu_id]
  end
end
