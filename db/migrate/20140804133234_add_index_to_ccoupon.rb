class AddIndexToCcoupon < ActiveRecord::Migration
  def change
    add_index :coupons, [:user_id, :tibu_tibu_id], unique: true, name: 'index_unique_coupon'
  end
end
