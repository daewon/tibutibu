class AddflagcolumnToReleaseNote < ActiveRecord::Migration
  def change
    add_column :release_notes, :notiable, :boolean, default: true
    add_index :release_notes, [:notiable], name: 'index_flag_release_note'
  end
end
