class CreateMappingTableUserCities < ActiveRecord::Migration
  def change
    create_table :user_cities do |t|
      t.references :user, index: true
      t.references :city, index: true
    end

    add_index :user_cities, [:user_id, :city_id], unique: true, name: 'index_unique_city'
  end
end
