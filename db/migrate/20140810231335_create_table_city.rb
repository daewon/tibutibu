class CreateTableCity < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.references :province, index: true
      t.integer :code
      t.string :name
    end
  end
end
