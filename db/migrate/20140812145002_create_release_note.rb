class CreateReleaseNote < ActiveRecord::Migration
  def change
    create_table :release_notes do |t|
      t.string :title, null: false
      t.string :content, null: false

      t.timestamps
    end

    add_column :users, :is_newbie, :boolean, default: true
    add_column :users, :visited_notice, :datetime
  end
end
