class CreateTableNotification < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :receiver, index: true
      t.references :tibu_tibu, index: true
      t.references :sender, index: true
      t.timestamps
    end

    add_column :users, :visited_notification, :datetime
  end
end
