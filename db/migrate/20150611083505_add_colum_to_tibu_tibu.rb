class AddColumToTibuTibu < ActiveRecord::Migration
  def change
    add_reference :tibu_tibus, :brand, index: true
    remove_column :tibu_tibus, :brand
  end
end
