class AddTokenToUser < ActiveRecord::Migration
  def change
    add_column :users, :expires_at, :datetime, default: nil
    add_column :users, :refresh_token, :string, default: nil
    add_column :users, :token, :string, default: nil
  end
end
