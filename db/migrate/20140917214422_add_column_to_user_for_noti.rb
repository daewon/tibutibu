class AddColumnToUserForNoti < ActiveRecord::Migration
  def change
    add_column :users, :group_notification, :boolean, default: true
    add_column :users, :location_notification, :boolean, default: true
  end
end
