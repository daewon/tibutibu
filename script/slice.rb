include Magick

img = ImageList.new("script/tibu.png")

output_images =
  (0..4).collect { |i|
    (0..9).collect { |j|
      x = (j * 202) + 1
      y = (i * 266) + 1
      img.crop(x, y, 202, 265)
    }
  }

output_images.flatten.each_with_index do |i, index|
  name = "script/sliced_image/#{index + 91}.jpg"
  i.write(name)
end
