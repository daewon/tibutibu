# coding: utf-8
require 'rest_client'
require 'json'
require 'nokogiri'
require 'mysql2'

info = {host: "ctom029.kr.iwilab.com", username: "develop", password: "devteam"}
env = "development"
venice = Mysql2::Client.new(info.merge(database: "venice_#{env}2"))
opt = {stream: true, cache_rows: false}

user_map = {}
agit_id_to_profile_image = {}
venice.query("select agit_id, image_url from users").each do |row|
  next if row["image_url"].nil?
  image_hash = JSON.parse row["image_url"]
  id = row["agit_id"]
  url = image_hash["600_600"] || image_hash["600_600"] || image_hash["o"]
  agit_id_to_profile_image[id.upcase] = url
end

(Dir.mkdir './sql' rescue nil)

# insert query maker
get_or_null = -> val do
  val ? "'#{Mysql2::Client.escape(val) rescue val}'" : "null"
end

make = -> source, table_name, columns, file_name=nil do
  file_name = file_name || table_name
  puts "work start: #{table_name} -> #{file_name}.sql ...."
  cnt = 0
  insert_query = -> values { "REPLACE #{table_name} (#{columns.join(',')}) values (#{values.join(',')})" }
  File.open("sql/#{file_name}.sql", "w") do |file|
    source.each { |r| file.puts("#{insert_query.(columns.map {|c| get_or_null.(r[c])})};"); cnt += 1 }
  end
  puts "work finish: #{table_name} -> #{file_name}.sql :#{cnt} items!\n"
end

URL = OpenStruct.new(
  {
    krews: "https://inhouse-sso.kakao.com/api/v2/krews",
    store: "http://www.diningcode.com/list.php" # params: ?page=2&chunk=10&query=판교
  })

# 크루 정보 만들기
krew_infos = {}
krews = JSON.parse(RestClient.get URL.krews)
fields = [:mobile_phone_number, :name, :email, :company_name, :talk_id, :gender_type]

cnt = 1
user_emails = {}
source = Enumerator::Lazy.new(krews) do |yielder, krew|
  url = agit_id_to_profile_image[krew["id"].upcase]
  next if krew["active"].to_s == "false"
  next if user_emails[krew["email"]]

  krew["encrypted_password"] = "#{rand(36**8).to_s(10)}"
  krew["image"] = url
  krew["phone_number"] = krew["mobile_phone_number"] || krew["office_phone_number"]
  krew["kakao_id"] = krew["talk_id"]
  krew["company_name"] = krew["company_name"]
  krew["gender"] = krew["gender_type"]
  krew["updated_at"] = Time.now
  user_emails[krew["email"]] = true

  krew["id"] = cnt
  user_map[cnt] = krew

  cnt += 1
  yielder.yield krew
end

table_name = "users"
columns = %W{id email encrypted_password name image kakao_id company_name phone_number gender updated_at}
make.(source, table_name, columns)

fetch_store = -> query do
  puts "fetch start: #{query}"
  html = Nokogiri::HTML(RestClient.get(URL.store, params: {query: query}).body)

  # parse page_count and set page_count 10 if page_count > 10
  page_count = html.css('#search_sum_tab .stat_h').text.scan(/\d+/).first.to_i / 10
  page_count = 10 if page_count > 10

  # parse xml and stores data
  stores = (1..page_count).each_with_object({}) do |n, hash|
    puts "fetch page: #{n}"

    html = Nokogiri::HTML(RestClient.get(URL.store, params: {page: n, chunk: 10, query: query}).body)
    items, bodies = html.css('.item_header'), html.css('.item_body')

    items.zip(bodies).each do |header, body|
      title, address, phone = [header.css('.r_title_cell_link').text, body.css('.addr_cell').text, body.css('.phone_cell').text]
      p [title, address, phone]
      hash[title] = [address, phone]
    end
  end
  puts "fetch finish"
  stores
end

splitter = "!!@@##"
# stores = fetch_store.("삼평동")
# stores = stores.merge fetch_store.("판교역")
# stores = stores.merge fetch_store.("판교")
# File.open("stores.txt", "w") do |file|
#   stores.each_with_index { |(k, v), i| file.puts([i+1, k, v].flatten.join(splitter)); }
# end

store_map = {}
# stores
source = Enumerator::Lazy.new(File.open("stores.txt").each_line) do |yielder, line|
  idx, name, address = line.split splitter
  row = {}
  row["id"] = idx
  row["name"] = name
  row["address_id"] = 1
  row["updated_at"] = Time.now

  store_map[idx] = row
  yielder.yield row
end

columns = %W{id name address_id updated_at}
make.(source, "stores", columns)

# address
source = Enumerator::Lazy.new([["판교", 1], ["강남", 2], ["종로", 3], ["선릉", 4], ["가산", 5]]) do |yielder, item|
  name, id = item
  row = {"name" => name, "id" => id, "updated_at" => Time.now}

  yielder.yield row
end

columns = %W{id name}
make.(source, "addresses", columns)

# coupons
coupons, cnt = [], 1
store_keys = store_map.keys
user_map.each do |id, user|
  keys = store_keys.sample (rand(10) + 2)
  keys.each do |k|
    item = { "user_id" => id, "store_id" => k, "stamp" => rand(9) + 1, "id" => cnt, "updated_at" => Time.now }
    coupons << item
    cnt += 1
  end
end

columns = %W{id user_id store_id stamp updated_at}
make.(coupons, "coupons", columns)
