source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

gem 'rest-client'
gem 'kaminari'
gem 'mysql2'

# web server
gem 'capistrano', '~> 3.1'
gem 'capistrano-rbenv', '~> 2.0'
gem 'capistrano-bundler', '~> 1.1.2'
gem 'capistrano-rails', '~> 1.1'

# HAML simple HTML builder
gem 'haml'

# less: css preprocessor
gem 'less-rails'
gem 'font-awesome-rails'

# css compressor
gem 'yui-compressor'

# unicode encoding
gem 'iconv'

gem 'devise'
gem 'oauth2'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem 'omniauth-kakao'

gem 'rmagick'
#gem 'activeadmin'

gem 'activerecord-import'
gem 'active_model_serializers'

group :development, :test do
  gem 'thin'
  gem 'database_cleaner'
  gem "spring"
  gem 'quiet_assets'

  # https://github.com/bmabey/database_cleaner
  gem 'sqlite3'
  gem 'activerecord-deprecated_finders'

  # better debugging
  # http://www.sitepoint.com/silver-bullet-n1-problem/
  gem 'bullet'
  gem 'logger-colors'
  gem 'better_errors'
  gem "binding_of_caller"
  gem 'pry'
  gem 'interactive_editor'
  gem 'awesome_print'
end
