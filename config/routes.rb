Coupan::Application.routes.draw do

  devise_for :users, controllers: {
      omniauth_callbacks: "users/omniauth_callbacks",
      :registrations => "registrations"
    }


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  resources :brands, only: [:index, :show] do
    resources :coupons, only: [:index]
  end

  resources :coupons # index is legacy url (for above brand_coupon)

  resources :release_notes, only: [:index] do
    collection do
      get 'recent'
    end
  end

  resources :notifications, only: [:index] do
    collection do
      get 'recent'
    end
  end

  #resources :exchanges, only: [:new, :create, :show]
  get '/brands/:brand_id/users/:id/coupons', to: 'coupons#me', as: 'brand_user_coupons'
  get '/users/:id/coupons', to: 'coupons#me', as: 'user_coupons' # legacy url(for above)

  get '/users/:id/coupons/:tibu_tibu_id/', to: 'coupons#recommend', as: 'recommend_coupons'
  post '/users/:id/coupons/post_kakao_story', to: 'coupons#post_kakao_story', as: 'post_kakao_story'
  get '/users/:id/coupons/:tibu_tibu_id/request_exchange', to: 'coupons#request_exchange', as: 'request_exchange_coupon'


  get '/oauth/', to: 'home#oauth_redirect'

  resource :api do
    member do
      get 'stores'
      get 'companies'
      get '/cities/:province_id/', to: 'apis#cities'
    end
  end

  get '/googlec41459c75d69b2af.html', to: 'home#google_valification'


  # legacy support
  resources :coupons


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
