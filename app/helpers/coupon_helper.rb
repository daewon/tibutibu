module CouponHelper
  def exchangerble_ssyls(coupons, user)
    html = "<ul class='list-unstyled list-inline'>"
    coupons.each do |coupon|
      html += "<li class='ssyl' style='background-image:url(#{coupon.image_path});'></li>"
    end
    html += "</ul>"

    if user.phone_number.present? or user.kakao_id.present? or user.user_cities.present?
      html += "<ul class='contact'>"
      if user.phone_number.present?
        html += "<li><label>연락처 :</label><a class='btn btn-link' href='tel:+#{user.phone_number}'>#{user.phone_number}</a></li>"
      end
      if user.kakao_id.present?
        html += "<li><label>KAKAO ID :</label><a class='btn btn-link'>#{user.kakao_id}</a></li>"
      end
      if user.user_cities.present?
        html += "<li><label>장소 :</label> #{user.cities_name}</li>"
      end
      html +="</ul>"
    end
    html
  end
end
