$(document).on("submit", ".stamp-form", function() {
  var $is_address = $("input[name='coupon[address]']");
  var $is_store = $("input[name='coupon[store]']");
  var $is_stamp = $("input[name='coupon[stamp][]']");

  if (!$is_address.is(':checked')) {
    alert("가게가 위치한 장소를 선택해주세요.");
    $is_address.parent().addClass('blink');
    return false;
  } else if (!$is_store.val().trim()) {
    alert("가게의 이름을 적어주세요.");
    $is_store.addClass('blink');
    return false;
  } else if (!$is_stamp.is(':checked')) {
    alert("받으신 스탬프를 꾹 눌러주세요.");
    $is_stamp.parent().addClass('blink');
    return false;
  }
})
