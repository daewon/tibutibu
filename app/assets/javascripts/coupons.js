//= require cropper.min.js
//= require scroll_auto_loader

$(document)
  .on('ajax:success', '#post-kakao-story', function(xhr, data, status) {
    $(this).modal('hide');
    $("#toast .message").text("성공적으로 포스팅 되었습니다")
    $("#toast").addClass("alert-success").fadeIn("1500", function() {
      $(this).delay(4000).fadeOut("3000", function() {
        $(this).removeClass("alert-success")
      });
    });
  })
  .on('ajax:fail', '#post-kakao-story', function(xhr, data, status) {
    $("#toast .message").text("포스팅하는데 문제가 있었습니다. 관리자에게 문의해 주세요.")
    $("#toast").addClass("alert-danger").fadeIn("1500", function() {
      $(this).delay(4000).fadeOut("3000", function() {
        $(this).removeClass("alert-danger")
      });
    });

  });

$(document).on('ajax:success', '.create-ssyl', function(xhr, data, status) {
  if ($(this).hasClass('with-exchange')) {
    var $wrapper = $(this).parents().closest('.exchange-ssyl-wrapper');
    $wrapper.before($(data)).remove();
  } else {
    $(this).before($(data)).remove();
  }
});

$(document).on('submit', '.update-ssyl', function(e) {
  //var formData = $(this).serialize();
  var formData = new FormData(this);
  var $f = $(this);

  $.ajax({
    method: "PUT",
    url: $f.attr('action'),
    data: formData,
    mimeType:"multipart/form-data",
    contentType: false,
    cache: false,
    processData:false,
    dataType: 'json'
  })
    .done(function(data, status) {
      var count = data.coupon.count;
      var id = data.coupon.id;
      var picture = data.coupon.picture;
      var $ssyl = $("#ssyl-" + id);

      $f.find("#counter").text(count);
      $f.find('#coupon_picture').next().text('사진 올리기');

      if (picture) {
        $f.find('.ssyl')
          .css("background-image", "url('" + picture + "')");
        $ssyl.find('img').attr('src', picture);
      }
      $ssyl.find(".count").text("+ " + count);
    })
    .fail(function() {
      $f.find("#coupon_count").val($f.find("#counter").text());
      alert('Something wrong update your coupon. Please try again.')
    })
    .always(function() {
    })
    ;

  return false;
});

$(document).on("change", "#coupon-toggle input", function() {
  var url = $(this).data('href');
  location.href = url;
  return false;
});

$(document).on("click", "button.minus", function() {
  var $f = $(this).parents().closest('form');
  var $count = $f.find("#coupon_count");
  var counter = parseInt($count.val()) - 1;
  if (counter < 1) {
    alert("씰의 최소갯수는 1 입니다.");
  } else {
    $count.val(counter);
    $f.find("#counter").text(counter);
    $f.submit();
  }

  return false;
});

$(document).on("change", "#coupon_picture", function() {
  var $f = $(this).parents().closest('form');
  $(this).next().text('사진 올리는 중..');
  $f.submit();
  return false;
});

$(document).on("click", "button.plus", function() {
  var $f = $(this).parents().closest('form');
  var $count = $f.find("#coupon_count");
  var counter = parseInt($count.val()) + 1;
  $count.val(counter);
  $f.submit();

  return false;
})


$(document).on("click", "#coupon-list .portfolio-sec", function() {
  var href = $(this).attr('href')
  if (!href) { return true; }
  location.href = href;
});

$(document).ready(function() {
  $('a[rel="popover"]').popover({ html: true });

  $('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
    $(this).find('textarea').val('');
  });
});
