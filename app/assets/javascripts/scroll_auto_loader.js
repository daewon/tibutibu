function scrollHandler() {
  if (($(document).height() - 1500) <= $(window).scrollTop() + $(window).height()) {
    var $pager = $("a[rel='next']")
    if ($pager.length) {
      loadNextPage();
    }
    else {
      $(window).unbind('scroll');
    }
  }
}

function loadNextPage() {
  var $pager = $("a[rel='next']");

  $.ajax({
    url: $pager.attr('href'),
    beforeSend: function(xhr) {
      $(window).unbind('scroll');
      $pager.text("다음 페이지를 로딩중입니다...")
    }
  })
    .done(function( data ) {
      $pager.remove();
      $('#coupon-list').append(data);
      if (data) {
        $(window).bind('scroll', function() {
          scrollHandler();
        });
      }
    });
}

$(document).ready(function() {
  var $pager = $("a[rel='next']");
  if ($pager.length) {
    $(window).bind('scroll', scrollHandler());
  }
})
