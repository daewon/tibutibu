//= require jquery.autocomplete.min

$(document).ready(function() {
  $('input.autocomplete').autocomplete({
    serviceUrl: '/api/companies'
  })
  ;

 $("#province").on("change", function() {
   var province_id = $(this).val();
   $.get("/api/cities/" + province_id, function(data) {
     var $city = $("#city");
     $city.empty();

     $.each(data, function(index, d) {
       $city.append($('<option value="' + d.id + '">' + d.name + '</option>'));
     });
   });
 });

  $("#add-city-form .btn-primary").on("click", function() {
    var city = $("#city").val();
    var $cities = $("input[name='user[cities][]']");
    var existed = false;

    if (!city) {
      alert('도시를 선택해 주세요!');
      return false;
    } else if ($cities.length >= 3) {
      alert('3개 이상의 장소를 선택할 수 없습니다!');
      return false;
    }

    $.each($cities, function(index, el) {
      if ($(el).val() == city) {
        alert('선택하신 도시는 이미 추가되었습니다. 다른 도시를 선택해 주세요.');
        existed = true;
      }
    });
    if (existed) { return false; }

    var province_name = $("#province option:selected").text();
    var city_name = $("#city option:selected").text();
    var $close_btn = $('<button type="button"></button>')
      .addClass('close').data('dismiss', 'alert').html('&times;');
    var $location = $('<span>' + province_name + ' / ' + city_name + '</span>');
    var $input = $('<input type="hidden" name="user[cities][]" />').val(city);

    $("#location").append(
      $('<div class="alert alert-info alert-dismissible"></div>')
        .append($close_btn).append($location).append($input)
    );
  });

  $("#notification-toggle .btn").button();
  $("#notification-toggle .btn-group label").on("click", function() {
    if (!$(this).find("input").is(":checked")) {
      $(this).parents().closest('.btn-group').find("input").attr("checked", false);
      $(this).find("input").attr( "checked", true );
    }
  })

});

