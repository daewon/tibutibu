//= require jquery
//= require jquery_ujs
//= require jquery.easing.1.3.min
//= require bootstrap.min


$(document).on("click", ".popover a.close", function() {
  $(this).parents().closest('.popover').remove();
});

$(function() {
  $("#notice[data-toggle='popover']").popover({
    html: true,
    container: ".navbar-fixed-top",
    title: "Announcement <a class='close pull-right' data-dismiss='popover'>&times;</a>",
    content: $(".spinner")
  })
  .on("show.bs.popover", function(e) {
    var $e = $(this);
    var url = $e.data('url');

    $.get(url)
      .done(function(data) {
        var $popover = $e.parents().find('.popover .popover-content');
        $.each(data, function(index, n) {
          var $n = $("<h4>" + n.title + "</h4><p>" + n.content + "</p>");
          $popover.append($n);
        });
        $popover.find(".spinner").remove();
        $e.removeClass('active').find('.title').remove();
      })
      .fail(function(data) {
        $popover.find('.popover-content').html('Sorry, Fail to profile load.');
      });
  })
  .on("hidden.bs.popover", function(e) {
    $(this).popover('destroy');
    $(this).attr('href', '/release_notes/');
  });

  $("#notification[data-toggle='popover']").popover({
    html: true,
    container: ".navbar-fixed-top",
    title: "New Exchangable Ssyl<a class='close pull-right' data-dismiss='popover'>&times;</a>",
    content: $(".spinner")
  })
  .on("show.bs.popover", function(e) {
    var $e = $(this);
    var url = $e.data('url');

    $.get(url)
      .done(function(data) {
        var $popover = $e.parents().find('.popover .popover-content');
        var $wrapper = $("<ul>").addClass("list-unstyled");

        if (data.length) {
          $.each(data, function(index, n) {
            var $profileImage = $("<img>")
              .addClass("profile-image media-object pull-left")
              .attr("src", n.sender.image);
            var $content = $("<span>")
              .text(n.sender.name + " 님이 " + n.tibu_tibu_id + "번 씰을 2개 이상 등록하였습니다. (" + n.ctime + ")")
            var $contents = $("<li>")
              .addClass("media pull-left")
              .append($profileImage, $content);
            $wrapper.append($("<a>").attr("href", "/users/" + n.sender.id + "/coupons").append($contents));
          });
        } else {
          var $contents = $("<li>")
            .addClass("media text-center")
            .text("근처에 씰을 교환할 추천인이 없습니다.");
          $wrapper.append($contents);
        }

        $popover.addClass("notification").append($wrapper);
        $popover.find(".spinner").remove();
        $e.removeClass('active').find('.title').remove();
      })
      .fail(function(data) {
        $popover.find('.popover-content').html('Sorry, Fail to profile load.');
      });
  });
});



(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-52460429-1', 'auto');
ga('send', 'pageview');
