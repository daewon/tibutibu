/*========tooltip and popovers====*/
$(function() {
  $("[data-toggle=popover]").popover();
  $("[data-toggle=tooltip]").tooltip();
});


/*========sticky nav====*/
$(function() {
  var stickyHeaderTop = $('.sticky-nav').offset().top;
  $(window).scroll(function() {
    if ($(window).scrollTop() > stickyHeaderTop) {
      $('.sticky-nav').css({position: 'fixed', top: '0px'});
      $('.sticky-nav').css('display', 'block');
    } else {
      $('.sticky-nav').css({position: 'static', top: '0px'});
    }
  });
});
