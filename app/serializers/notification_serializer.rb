class NotificationSerializer < ActiveModel::Serializer
  attributes :id, :ctime

  #has_one :receiver, serializer: UserSerializer
  has_one :tibu_tibu_id
  has_one :sender, serializer: UserSerializer

  def ctime
    object.created_at.strftime("%m. %d")
  end
end
