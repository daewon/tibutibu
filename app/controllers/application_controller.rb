class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate_user!

  before_action :set_brand

  rescue_from Exception do |ex|
    reset_session
    raise ex
  end

  def login_required
    unless user_signed_in?
    end
  end


  private def set_brand
    brand_id = params.fetch(:brand_id, nil)
    @brand = if brand_id.present?
               Brand.find(params.fetch(:brand_id, nil))
             elsif session["brand_id"].present?
               Brand.find(session["brand_id"])
             else
               Brand.last
             end
  end


end
