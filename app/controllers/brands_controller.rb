class BrandsController < ApplicationController

  skip_before_action :authenticate_user!

  def index
    @brands = Brand.all
  end

  def show
    @brand = Brand.find(params.fetch(:id, nil))
    session["brand_id"] = @brand.id

    redirect_to brand_coupons_path(@brand.id)
  end

end
