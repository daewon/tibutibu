class ExchangesController < ApplicationController

  #before_action :tibutibus, only: [:index, :me]

  def show
  end

  def new
    @user = User.find params[:id]
    @coupon = Coupon.find_by(
      tibu_tibu_id: params[:tibu_tibu_id],
      user_id: params[:id]
    )
    @candidate_coupons = @user.recommended_coupons_with(current_user)

    render layout: false
  end

  def create
  end

end
