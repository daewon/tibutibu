class NotificationsController < ApplicationController

  def index
  end

  def recent
    notifications = Notification
      .eager_load(:tibu_tibu)
      .where(receiver_id: current_user.id)
      .where("tibu_tibus.brand_id = ?", @brand.id)
      .order(created_at: :desc)

    last_visited_time = current_user.visited_notification ||= ''

    @notifications = notifications.limit 10
    current_user.update(visited_notification: Time.now)

    render json: @notifications, each_serializer: NotificationSerializer
  end

end
