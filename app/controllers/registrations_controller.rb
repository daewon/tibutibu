class RegistrationsController < Devise::RegistrationsController

  def edit
    @provinces = Province.all
    province = @provinces.first
    @cities = if province.present?
                City.where(province_id: province.id)
              else
                []
              end
  end

  def update
    user = User.find current_user
    user.update user_params
    cities_params

    flash[:user_update] = "사용자 정보를 정상적으로 저장하였습니다."
    return redirect_to edit_user_registration_path
  end


  private def user_params
    company = Company.find_or_create_by(company_params)
    cities = cities_params
    params.require(:user)
      .permit(:phone_number, :kakao_id, :location_notification, :group_notification)
      .merge(company_id: company.id)
  end

  private def cities_params
    p_cities = params.require(:user)[:cities]
    p_cities = p_cities.collect{|s| s.to_i} if p_cities.present?
    cities = City.where(id: p_cities)
    current_user.update(cities: cities)
  end

  private def company_params
    cp = params.require(:company).permit(:name)
    cp[:name] = cp[:name].upcase.sub(/\s+\(\d+명\)/, "")
    cp
  end

end
