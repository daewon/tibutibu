class ReleaseNotesController < ApplicationController

  def index
    @notes = ReleaseNote.all.order(id: :desc)
  end

  def recent
    @notes = ReleaseNote.
      where(notiable: true).
      order(id: :desc)

    current_user.update(visited_notice: Time.now)

    render json: @notes
  end

end
