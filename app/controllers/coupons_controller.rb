require 'RMagick'
require 'rest_client'

class CouponsController < ApplicationController
  include Magick

  skip_before_action :authenticate_user!, only: [:index, :me, :show]

  before_action :tibutibus, only: [:index, :me]


  def index
    # legacy support (kakao friends coupon links)
    redirect_to brand_coupons_path(1) and return if params.fetch(:brand_id, nil).nil?

    query = params[:q]
    @users = User.joins(:coupons)
      .where(coupons: { brand_id: @brand.id })
      .group("users.id")
      .select("users.*")
      .select("COUNT(coupons.id) as c_count")
      .select("COUNT(coupons.picture) as p_count")
      .order("p_count desc", "c_count desc")

    @legend_users = @users.
      having("COUNT(coupons.picture) = ?", @brand.max_coupon_count)

    @users = @users.
      having("COUNT(coupons.picture) < ?", @brand.max_coupon_count).
      order("p_count desc", "c_count desc")

    if query
      if query == 'l'
        my_location = current_user.cities.pluck(:id)
        @users = @users.joins(:cities).where('cities.id in (?)', my_location)
      elsif query == 'g'
        @users = @users.where(company: current_user.company_id)
      end
    end

    @whole_ssyl_count = TibuTibu.where(brand_id: @brand.id).count
    @users = @users.page(params[:page]).per(15)

    if request.xhr?
      return render 'coupons/_list', layout: false
    end

  end

  def destroy
    coupon = Coupon.find params[:id]
    coupon.destroy! if current_user == coupon.user

    redirect_to user_coupons_path(current_user)
  end

  def show
    @coupon = Coupon.find params[:id]
    @recommend_coupons = @coupon.recommend
  end

  def edit
    @coupon = Coupon.find params[:id]
    render layout: false
  end

  def update
    @coupon = Coupon.find params[:id]

    if params[:coupon][:picture]
      path = "/picture/ssyls/#{self.current_user.id}/"
      if !File.directory?("public" + path)
        Dir.mkdir("public" + path)
      end

      temp_image = params[:coupon][:picture]
      image = Magick::Image.from_blob(temp_image.read).last
      image = image.resize_to_fill(150, 200)
      path = path + 'ssyl-' + SecureRandom.uuid + "." + image.format
      image.write("public" + path) do self.quality = 60 end
      @coupon.update(picture: path)
    end

    prev_count = @coupon.count
    count = params[:coupon][:count].to_i
    if current_user == @coupon.user
      if count == 0
        @coupon.destroy
      else
        @coupon.update(count: count)
      end

      if prev_count == 1 and count == 2
        _citizen = current_user.citizen.where(location_notification: true)
        had_citizen = _citizen
          .eager_load(:coupons)
          .where("coupons.tibu_tibu_id = ?", @coupon.tibu_tibu_id)
          .where("coupons.brand_id = ?", @coupon.brand_id)
        citizen = _citizen.pluck(:id) - had_citizen.pluck(:id)

        _colleague = current_user.colleague.where(group_notification: true)
        had_colleague = _colleague
          .eager_load(:coupons)
          .where("coupons.tibu_tibu_id = ?", @coupon.tibu_tibu_id)
          .where("coupons.brand_id = ?", @coupon.brand_id)
        colleague = _colleague.pluck(:id) - had_colleague.pluck(:id)

        notifier = (citizen + colleague).uniq
        queries = notifier.map do |receiver|
          Notification.new({
            sender_id: current_user.id,
            tibu_tibu_id: @coupon.tibu_tibu_id,
            receiver_id: receiver
          })
        end

        Notification.import(queries)
      end
    end

    render json: { coupon: @coupon }
  end

  def new
  end

  def create
    coupon = Coupon.new(coupon_params)
    if coupon.save
      if request.xhr?
        @user = current_user
        return render partial: 'coupons/me/active_ssyl', locals: {coupon: coupon, has_ssyl: true }
      else
        return redirect_to user_coupons_path current_user
      end
    end

    render 'coupon/new'
  end


  def me
    # legacy support (kakao friends coupon links)
    redirect_to brands_path and return if params.fetch(:brand_id, nil).nil?

    @user = User.find params[:id]
    @user_coupons = @user.coupons.where(brand: @brand)
    @user_ssyl_ids = @user_coupons.pluck(:tibu_tibu_id)

    if current_user
      @my_ssyl_ids = current_user.coupons.pluck(:tibu_tibu_id)
    end
  end

  def recommend
    @user = User.find params[:id]

    if @user == current_user
      @tibutibu = TibuTibu.find(params[:tibu_tibu_id])
      @recommend_coupons = @tibutibu.has_many_coupons
    else
      coupons = Coupon.find_by(
        tibu_tibu_id: params[:tibu_tibu_id],
        user_id: params[:id]
      )
    end

    render layout: false
  end

  def request_exchange
    @user = User.find params[:id]
    @coupon = Coupon.find_by(
      tibu_tibu_id: params[:tibu_tibu_id],
      user_id: params[:id]
    )
    @candidate_coupons = @user.recommended_coupons_with(current_user)

    render layout: false
  end

  def post_kakao_story
    current_user.token_refresh if current_user.token_expired?

    link = Rails.application.routes.url_helpers.user_coupons_url(
      current_user.id, host: request.host_with_port)

    result = RestClient.post(
      "https://kapi.kakao.com/v1/api/story/post/note",
      "content=#{params[:content]}\n\n#{link}",
      "Authorization" => "Bearer #{current_user.token}"
    )

    if result['id']
      return render json: result
    else
      return render json: {}, status: 500
    end
  end


  private def tibutibus
    @tibutibus = TibuTibu.where(brand: @brand)
  end

  private def coupon_params
    user = current_user
    params.require(:coupon).permit(:tibu_tibu_id).merge(user: user, brand: @brand)
  end

end
