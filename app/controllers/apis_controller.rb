require 'rest_client'

class ApisController < ApplicationController
  @@cache = ActiveSupport::Cache::MemoryStore.new(expires_in: 10.seconds)

  def cities
    cities = City.where(province_id: params[:province_id])
    render json: cities
  end

  def stores
    query = params[:query]
    stores = @@cache.fetch("stores") do
      Store.all.select(:id, :name, :address_id).map do |s|
        { value: s[:name], data: s.slice(:id, :address_id) }
      end
    end

    render json: {
      suggestions: stores.select { |s| s[:value].include? query }
    }
  end

  def companies
    query = params[:query].upcase

    companies = @@cache.fetch("compnaies") do
      User.
        joins(:company).
        group("users.company_id").
        where.not(name: ["", nil]).
        select("companies.name").
        select("COUNT(users.company_id) as c_count").map do |s|
          name = s.name.upcase
          { value: "#{name} (#{s.c_count}명)", data: name }
      end
    end

    render json: {
      suggestions: companies.select { |s| s[:value].include? query }
    }
  end

  def send_to_agit_message message
    RestClient.post(
      'https://inhouse-agit.kakao.com/internals/write',
      token: Rails.configuration.AGIT_TOKEN,
      message: message
    )
  end

end
