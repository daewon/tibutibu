class ReleaseNote < ActiveRecord::Base
  def self.last_updated_at
    ReleaseNote.all.last.updated_at
  end
end
