class City < ActiveRecord::Base
  belongs_to :province
  has_many :user_cities
  has_many :users, through: :user_cities
end
