# 164,1512,3698,155,3556,
# 3104
class Coupon < ActiveRecord::Base

  belongs_to :user
  belongs_to :tibu_tibu
  belongs_to :brand

  validates_uniqueness_of :user_id, scope: :tibu_tibu_id


  def recommend
  end

  def self.check_status(user_id)
    # stores = {}
    # for (store_id, user_with_scores) in Coupon.recommend(1, receive_coupon_ids)
    #   stores[store_id] = !user_with_scores.empty?
    # end
    # stores
    coupons = Coupon.where(user_id: user_id)
    stores = {}
    for my_coupon in coupons
      stores[my_coupon.store_id] =
        Coupon.where(store_id: my_coupon.store_id)
        .where("stamp >= ?", my_coupon.store.max_stamp - my_coupon.stamp).count == 0 ? false : true
    end
    stores
  end

  def image_path
    if self.picture
      self.picture
    else
      if self.tibu_tibu.brand_id == 1
        url = 'ssyls/' + self.tibu_tibu.brand_id.to_s + '/' + self.tibu_tibu.num.to_s + '.jpg'
        ActionController::Base.helpers.asset_path(url)
      else
        nil
      end
    end
  end

end
