class Notification < ActiveRecord::Base
  belongs_to :receiver, class_name: "User"
  belongs_to :tibu_tibu
  belongs_to :sender, class_name: "User"
end
