class TibuTibu < ActiveRecord::Base

  belongs_to :brand

  has_many :coupons


  def has_many_coupons
    self.coupons.eager_load(:user).where("count > 1")
  end
end
