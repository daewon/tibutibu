class Store < ActiveRecord::Base
  belongs_to :address

  validates :name, uniqueness: {
    scope: [:address_id, :max_stamp], case_sensitive: false
  }

  def name_with_address
    "%s (%s)" % [self.name.upcase, self.address.name]
  end

  def name
    self[:name].upcase
  end

  def name=(name)
    self[:name] = name.upcase
  end

end
