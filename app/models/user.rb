class User < ActiveRecord::Base
  has_many :coupons
  has_many :notifications, foreign_key: "receiver_id"
  has_many :user_cities
  has_many :cities, through: :user_cities

  belongs_to :company
  accepts_nested_attributes_for :company

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, omniauth_providers: [:facebook, :twitter, :kakao]


  def getting_coupons(brand_id)
    self.coupons.where(brand_id: brand_id)
  end

  def recommended_coupons(brand_id)
    Coupon
      .where(brand_id: brand_id)
      .where('count > 1')
      .where.not(user: self)
      .where.not(tibu_tibu_id: getting_coupons(brand_id).pluck(:tibu_tibu_id))
  end

  def recommended_coupons_count(brand_id)
    self.recommended_coupons(brand_id).select('distinct tibu_tibu_id').count
  end

  def recommended_coupons_with(user, brand_id)
    recommended_coupons = self.recommended_coupons(brand_id).where(user: user)
  end

  def recommended_coupons_count_with(user, brand_id)
    self.recommended_coupons_with(user, brand_id).select('distinct tibu_tibu_id').count
  end

  def getting_coupons_count(brand_id)
    getting_coupons(brand_id).count
  end

  def confirmed_coupons_count(brand_id)
    getting_coupons(brand_id).where.not(picture: nil).count
  end

  def remaining_coupons_count(brand_id)
    whole_tibutibu_count = TibuTibu.where(brand_id: brand_id).count
    whole_tibutibu_count - self.getting_coupons(brand_id).count
  end

  def last_created_coupon(brand_id)
    getting_coupons(brand_id).order(:created_at).last
  end

  def token_expired?
    (self.expires_at.nil? || Time.now >= self.expires_at)
  end


  def token_refresh
    result = RestClient.post(
      'https://kauth.kakao.com/oauth/token',
      grant_type: 'refresh_token',
      client_id: Rails.configuration.KAKAO_APP_ID,
      refresh_token: self.refresh_token
    )

    token = JSON.parse result
    self.update(
      token: token['access_token'],
      expires_at: Time.at(Time.now.to_i + token['expires_in']),
      refresh_token: token['refresh_token'] || self.refresh_token,
    )
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.kakao"] && session["devise.kakao"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end



  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = (auth.uid + auth.provider) unless user.id
      user.name = auth.info.name   # assuming the user model has a name
      user.image = auth.info.image # assuming the user model has an image
      user.remember_me = true
      user.save!
    end
  end

  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.uid+"@twitter.com"
      user.password = (auth.uid + auth.provider) unless user.id
      user.name = auth.info.name   # assuming the user model has a name
      user.image = auth.info.image # assuming the user model has an image
      user.remember_me = true
      user.save!
    end
  end

  def self.find_for_kakao_oauth(auth, signed_in_resource=nil)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.token = auth.credentials.token
      user.refresh_token = auth.credentials.refresh_token
      user.expires_at = Time.at auth.credentials.expires_at
      user.uid = auth.uid
      user.email = auth.uid+"@kakao.com"
      user.password = (auth.uid + auth.provider) unless user.id
      user.name = auth.info.name   # assuming the user model has a name
      user.image = auth.info.image # assuming the user model has an image
      user.remember_me = true
      user.save!
    end
  end

  def is_empty_info?
    if not self.phone_number.present? and not self.agit_id.present? and not self.kakao_id.present?
      return true
    end
    return false
  end

  def cities_name
    self.cities.pluck(:name).join(" / ")
  end

  def citizen
    User.joins(:cities).
      where("cities.id in (?)", self.cities.pluck(:id)).
      where.not("users.id = ?", self.id).
      distinct
  end

  def colleague
    User.where(company: company)
      .where.not(company: nil)
      .where.not("users.id = ?", self.id)
  end

  def unread_notification
    if visited_notification
      notifications.where("created_at > ?", visited_notification)
    else
      notifications
    end
  end

  def has_unread_notification
    self.unread_notification.exists?
  end

end
